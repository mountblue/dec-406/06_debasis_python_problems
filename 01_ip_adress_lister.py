import ipaddress

def list_all_ip_adress(address):
    network = ipaddress.ip_network(address)
    for i in network.hosts():
        i = str(i)
        print(i)

address = "198.108.100.0/24"
list_all_ip_adress(address)