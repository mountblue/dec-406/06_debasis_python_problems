import imageio
import glob

numpyArray = []
images = glob.glob("./images/*.jpeg")

for file in images:
    numpyArray.append(imageio.imread(file))
imageio.mimsave("./images/gify.gif", numpyArray)
    