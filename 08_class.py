class String(object):

    def __init__(self, ipt = None):
        self.ipt = ipt
    
    def getInput(self):
        if self.ipt != None:
             return  self.ipt.upper()
        else:
            ask = input("Give a text ? ")
            self.ipt = ask
            return self.ipt.upper()


s = String()
print(s.getInput())

