def makeMatrix(m,n):
    return [[j*i for j in range(n)] for i in range(m)]

print(makeMatrix(3,5))