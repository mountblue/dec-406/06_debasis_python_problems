from collections import Counter

def remove_duplicate():
    get_sentence = input("write your sentence ?")
    frequency = Counter([word for word in get_sentence.split(" ")])
    return " ".join([key for key in frequency.keys()])

print(remove_duplicate())