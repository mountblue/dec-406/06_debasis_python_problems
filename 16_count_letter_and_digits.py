import re

def count_nums_and_digits(sentence):
    pattern_word = re.compile("[a-zA-z]*")
    patter_digit = re.compile("[0-9]*")
    letters = [pattern_word.match(word).group() for word in sentence]
    digits = [patter_digit.match(word).group() for word in sentence]
    print("LETTERS: {}, DIGITS: {}".format(len("".join(letters)), len("".join(digits))))

count_nums_and_digits("hello world ! 123")