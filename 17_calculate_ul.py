import re

def calculate_upper_lower(sentence):
    patternUpper = re.compile("[A-Z]*")
    patternLower = re.compile("[a-z]*")
    upper_letters = "".join([patternUpper.match(word).group() for word in sentence])
    lower_letters = "".join([patternLower.match(word).group() for word in sentence])
    print(f"UPPER: {len(upper_letters)}, LOWER: {len(lower_letters)}")

calculate_upper_lower("hello World HI")