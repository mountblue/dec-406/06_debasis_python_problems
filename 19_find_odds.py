def find_odds(ls):
    odds = [num**2 for num in ls if num % 2 == 1]
    return odds