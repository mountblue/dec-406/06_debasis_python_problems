import re

def password_validator(password):
    lower = []
    upper = []
    digit = []
    spChar = []
    speacialChars = "#$@"
    if 6 <= len(password) <= 12 :
        for char in password:
            if char.islower():
                lower.append(char)
            elif char.isupper():
                upper.append(char)
            elif char.isdigit():
                digit.append(char)
            elif speacialChars.find(char) != -1:
                spChar.append(char)
        return len(lower) >= 1 and len(upper) >= 1 and len(digit) >= 1 and len(spChar) >= 1
    return False

def check_passwords(*args):
    right_password = []
    for password in args:
        if password_validator(password):
            right_password.append(password)
    print(*right_password, sep=',')

check_passwords("helloThere1$","shhhs","Hej098$jjjjdjjjajjjajjjjsjjjs")