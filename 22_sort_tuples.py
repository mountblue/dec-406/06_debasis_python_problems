def get_tuples():
    list_of_tuples = []
    while True:
        getInput = input("Add a name, age and score ? (name age score)- ")
        if(len(getInput)):
            list_of_tuples.append(tuple(getInput.split(" ")))
        else:
            break
    return list_of_tuples


def sort_tuples():
    list_of_tuples = get_tuples()
    list_of_tuples.sort(key = lambda x: x[0])
    list_of_tuples.sort(key = lambda x: int(x[1]))
    list_of_tuples.sort(key = lambda x: int(x[2]))
    print(list_of_tuples)

sort_tuples()