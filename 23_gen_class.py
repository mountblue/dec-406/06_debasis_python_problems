class Generator(object):
    def __init__(self, n):
        self.n = n
    
    def divide_by_seven(self):
        for num in range(0, self.n):
            if num % 7 == 0:
                yield num

for num in Generator(100).divide_by_seven():
    print(num)