# formula for distance between two points [(pos(x)**2 + pos(y)**2)**0.5]

def get_dirction_and_step():
    steps = []
    while True:
        dir_and_step = input("Give direction and num of steps ? (D , S) ")
        if len(dir_and_step):
            D, S = dir_and_step.split(",")
            steps.append([D, int(S)])
        else:
            break
    return steps

def distance_robo_covered():
    steps = get_dirction_and_step()
    position = {
        "x":0,
        "y":0
    }

    for sd in steps:
        if sd[0] == "U":
            position["y"] += sd[1]
        elif sd[0] == "D":
            position["y"] -= sd[1]
        elif sd[0] == "R":
            position["x"] += sd[1]
        elif sd[0] == "L":
            position["x"] -= sd[1]
    distance = round((position["x"]**2 + position["y"]**2)**0.5)
    print(distance)

distance_robo_covered()