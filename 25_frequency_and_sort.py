from collections import Counter

def frequency_and_sort():
    get_sentence = input("Write a sentence ? ")
    sortedWords = sorted(get_sentence.split(" "))
    for (k, v) in Counter(sortedWords).items():
        print("{}:{}".format(k,v))

frequency_and_sort()
