def odd_or_even(num):
    if num % 2 == 0:
        print("{} is even".format(num))
    else:
        print("{} is odd".format(num))