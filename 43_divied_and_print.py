def divide_and_print():
    tpl = (1,2,3,4,5,6,7,8,9,10)
    mid = len(tpl) // 2
    print("{} \n {}".format(tpl[:mid+1], tpl[mid:]))