class Shape(object):

    def __init__(self, length):
        self.length = length

    def calc_area(self):
        return 0        

class Square(Shape):

    def __init__(self, length):
        super.__init__(self, length)
    
    def calc_area(self):
        return self.length ** 2