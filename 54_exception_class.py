class Myexception(Exception):

    def __init__(self, string):
        self.string = string

    def __str__(self):
        return super().__str__()


exp = Myexception("new error")
print(exp)