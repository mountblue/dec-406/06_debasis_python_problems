# formula : f(n) = f(n-1)+100 and n > 0
# n = 0  then f(0) = 0

def formula(n):
    if n == 0:
        return 0
    return formula(n-1) + 100

print(formula(5))