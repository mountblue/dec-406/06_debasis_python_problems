def generate_even(num):
    for i in range(num+1):
        if i % 2 == 0:
            yield i

print([num for num in generate_even(10)])