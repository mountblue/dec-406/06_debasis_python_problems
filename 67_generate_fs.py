def divisible_by_five_or_seven(num):
    for i in range(0, num):
        if i % 5 == 0 and i % 7 == 0:
            yield i
    
print([ num for num in divisible_by_five_or_seven(100)])