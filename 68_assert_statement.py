def assert_evens(ls):
    filtered_even = list(filter(lambda x: x % 2 == 0, ls))
    assert len(filtered_even) == len(ls), "This list does not include all even"
    return filtered_even
print(assert_evens([2,4,6,3,10]))