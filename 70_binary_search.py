def binary_sreach(sorted_list, value):
    if len(sorted_list) == 0:
        return False
    
    midpoint = len(sorted_list) // 2

    if sorted_list[midpoint] == value:
        return True
    elif sorted_list[midpoint] < value:
        return binary_sreach(sorted_list[midpoint+1:], value)
    else:
        return binary_sreach(sorted_list[:midpoint], value)

print(binary_sreach([1,2,3,4,5,6,7],5))
