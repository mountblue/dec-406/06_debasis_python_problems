import random

def random_even():
    ls = [ num for num in range(0,11) if num % 2 == 0]
    return random.choice(ls)

print(random_even())