import random

def random_choice():
    ls = [ num for num in range(0,11) if num % 5 == 0  and num % 7 == 0]
    return random.choice(ls)