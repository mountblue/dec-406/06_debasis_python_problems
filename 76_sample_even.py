import random

def sample_even():
    ls = [ num for num in range(100, 201) if num % 2 == 0]
    return random.sample(ls,5)