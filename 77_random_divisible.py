import random 

def random_divisible_by_fs():
    ls = [num for num in range(1,1001) if num % 5 == 0 and num % 7 == 0]
    return random.sample(ls, 5)