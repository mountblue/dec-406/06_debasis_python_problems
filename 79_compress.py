import zlib


def compress(strs):
    s = str.encode(strs)
    s = bytes(s)
    value = zlib.compress(s)
    return value

hextring = compress("hello world!hello world!hello world!hello world!")

def decompress(hextring):
    s = zlib.decompress(hextring).decode("utf8")
    print(s)

decompress(hextring)
