import timeit

code = """
def calc_runtime():
    num = 100
    while num:
        print(1+1)
        num -= 1
"""
print(timeit.timeit(stmt=code))
    