import random

def shuffle_the_list(ls):
    return random.shuffle(ls)