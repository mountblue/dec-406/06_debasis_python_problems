def make_sentence():
    subject = ["I", "You"]
    verb = ["Play", "Love"]
    objects = ["Hockey", "Football"] 
    sen = list(zip(subject, verb, objects))
    comp_sen = [" ".join(tpl) for tpl in sen]
    print(comp_sen)

make_sentence()