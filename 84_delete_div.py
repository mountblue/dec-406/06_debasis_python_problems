def delete_divisible(ls):
    ls = [num for num in ls if num % 5 != 0 and num % 7 != 0]
    return ls