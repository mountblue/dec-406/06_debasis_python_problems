def delete_even_index_value(ls):
    ls = [ num for i, num in enumerate(ls) if i % 2 != 0]
    return ls