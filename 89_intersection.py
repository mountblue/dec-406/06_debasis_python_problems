def intersection(ls1, ls2):
    return set(ls1).intersection(set(ls2))