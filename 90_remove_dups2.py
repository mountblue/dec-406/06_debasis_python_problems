def remove_duplicates(ls):
    return list(set(ls)).reverse()